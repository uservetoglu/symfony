<?php


/**
 * Class CartController
 * @package App\Controller
 * @author Üveys SERVETOĞLU <uveysservetoglu@gmail.com>
 */

namespace App\Controller;

use App\Entity\CartItem;
use App\Service\CartService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;

class CartController extends BaseController
{
    /**
     * @Route("/addToCart", name="add_to_cart", methods={"POST"})
     * @param Request $request
     * @param CartService $service
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function addToCart(Request $request, CartService $service)
    {
        $content = json_decode($request->getContent());

        $service->addToCart($content->product, $content->quantity, $this->getUser());

        return parent::response([], 'msg.success.cart.add_to_basket', 200);
    }

    /**
     * @Route("/removeCartItem/{cartItem}", name="cart_item_remove")
     * @param CartItem $cartItem
     * @param CartService $service
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeCartItem(CartItem $cartItem, CartService $service)
    {
        $service->removeItem($cartItem);
        return $this->redirectToRoute('cart');
    }

    /**
     * @Route("/cart", name="cart")
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function cart()
    {
        return $this->render('web/cart/index.html.twig', ['cart'=>parent::cart()]);
    }
}
