<?php

namespace App\Controller;

use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;

class ExceptionController extends BaseController
{
    public function exception(Request $request, FlattenException $exception, DebugLoggerInterface $logger = null)
    {
        return $this->render('error.html.twig',
            [
                'message'=>$this->translator->trans($exception->getMessage()),
                'code'=>$exception->getStatusCode()
            ]
        );
    }
}
