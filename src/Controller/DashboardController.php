<?php

namespace App\Controller;

use App\Entity\Payment;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    /**
     * @Route("/admin/dashboard", name="dashboard")
     * @return Response
     */
    public function dashboard()
    {
        $carts = $this->getDoctrine()
            ->getRepository(Payment::class)->findAll();
        return $this->render('dashboard/dashboard.html.twig', ['orders'=>$carts]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
