<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductFormType;
use App\Service\FileService;
use App\Util\File\FileKey;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProductController extends AbstractController
{
    /**
     * @Route("/product-list", name="home_products", methods={"GET"})
     * @return Response
     */
    public function list()
    {
        $products = $this->getDoctrine()->getRepository(Product::class)->findAll();
        return $this->render('web/product/index.html.twig', ['products' => $products]);
    }

    /**
     * @Route("/admin/products/new", name="product_new")
     * @param Request $request
     * @param FileService $service
     * @return Response
     * @throws \Exception
     */
    public function new(Request $request, FileService $service)
    {
        $product = new Product();
        $form = $this->createForm(ProductFormType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $service->upload($form->get('image')->getData(),FileKey::PR, true);
            $product->setImage($file[0]['fileName']);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();
            $this->addFlash('success', 'Successfully Created');
            return $this->redirectToRoute('products');
        }

        return $this->render('product/new.html.twig', [
            'productForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/products/{product}", name="product_edit", methods={"GET"})
     * @param $product
     * @return Response
     */
    public function edit(Product $product)
    {
        return $this->render('product/edit.html.twig', ['product' => $product]);
    }

    /**
     * @Route("/admin/products/{product}", name="product_update")
     * @param Product $product
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param FileService $service
     * @return Response
     * @throws \Exception
     */
    public function update(Product $product, Request $request, ValidatorInterface $validator, FileService $service)
    {
        if (!is_null($request->files->all()['image'])) {
            $file = $service->upload($request->files->all()['image'],FileKey::PR, true);
            $product->setImage($file[0]['fileName']);
        }
        $product->setName($request->get('name'));
        $product->setCode($request->get('code'));
        $product->setStock($request->get('stock'));


        if(count($errors = $validator->validate($product))>0){
            return $this->render('product/edit.html.twig', ['product' => $product, 'errors'=> $errors]);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($product);
        $em->flush();

        return $this->redirectToRoute('products');
    }

    /**
     * @Route("/admin/products/delete/{product}", name="product_delete")
     * @param $product
     * @return Response
     */
    public function delete(Product $product)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($product);
        $em->flush();

        return $this->redirectToRoute('products');
    }
}
