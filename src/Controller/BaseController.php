<?php


/**
 * Class BaseController
 * @package App\Controller
 * @author Üveys SERVETOĞLU <uveysservetoglu@gmail.com>
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\Translation\TranslatorInterface;

class BaseController extends AbstractController
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param array $result
     * @param null $message
     * @param int $code
     * @return JsonResponse
     */
    protected function response($result = [], $message = null, $code = 200)
    {
        $data = [];

        if (count($result) > 0) {
            $data['data'] = $result;
        }

        if (!is_null($message)) {
            $data['message'] = $this->translator->trans($message);
        }

        return new JsonResponse($data,$code);
    }

    protected function cart()
    {
        $data = $this->getUser()->getCarts()
            ->filter(
                function ($item) {
                    if($item->getStatus() == 'active') {
                        return $item;
                    }
                }
            );
        $data = array_values($data->toArray());

        if (empty($data)) {
            throw new \Exception('msg.error.cart.not_found');
        }
        return $data[0];
    }
}
