<?php


/**
 * Class CartController
 * @package App\Controller
 * @author Üveys SERVETOĞLU <uveysservetoglu@gmail.com>
 */

namespace App\Controller;

use App\Entity\Payment;
use App\Service\PaymentService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;


class PaymentController extends BaseController
{
    /**
     * @Route("/checkout", name="checkout")
     */
    public function checkout()
    {
        return $this->render('web/cart/checkout.html.twig', ['cart'=>parent::cart()]);
    }

    /**
     * @Route("/payment", name="payment")
     * @param Request $request
     * @param PaymentService $service
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function payment(Request $request, PaymentService $service)
    {
        $cart = parent::cart();

        if (is_null($cart) || $cart->getStatus() !== 'active') {
            return $this->redirectToRoute('home_products');
        }

        $payment = $service->payment($this->getUser(), $cart , $request->request->all());
        return $this->render('web/cart/payment.html.twig', ['orderNumber'=>$payment]);
    }

    /**
     * @Route("/payment/{payment}", name="payment_patch", methods={"PATCH"})
     * @param Payment $payment
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function orderStatus(Payment $payment, Request $request)
    {
        $content = json_decode($request->getContent());
        $payment->setStatus($content->status);
        $this->getDoctrine()->getManager()->persist($payment);
        $this->getDoctrine()->getManager()->flush();

        return parent::response([], 'msg.success.payment.status.'.$content->status, 200);
    }
}
