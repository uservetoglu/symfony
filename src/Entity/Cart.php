<?php

namespace App\Entity;

use App\Repository\CartRepository;
use App\Traits\EntityDateInformationTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CartRepository::class)
 */
class Cart
{
    use EntityDateInformationTrait;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="carts")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CartItem", mappedBy="cart")
     */
    private $items;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $totalQuantity;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $totalPrice;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $totalItems;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('completed', 'active', 'inactive')")
     */
    private $status;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Cart", inversedBy="payment")
     */
    private $payment;


    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user=$user;
    }

    /**
     * @return mixed
     */
    public function getTotalQuantity()
    {
        return $this->totalQuantity;
    }

    /**
     * @param mixed $totalQuantity
     */
    public function setTotalQuantity($totalQuantity): void
    {
        $this->totalQuantity=$totalQuantity;
    }

    /**
     * @return mixed
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * @param mixed $totalPrice
     */
    public function setTotalPrice($totalPrice): void
    {
        $this->totalPrice=$totalPrice;
    }

    /**
     * @return mixed
     */
    public function getTotalItems()
    {
        return $this->totalItems;
    }

    /**
     * @param mixed $totalItems
     */
    public function setTotalItems($totalItems): void
    {
        $this->totalItems=$totalItems;
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @param $product
     * @return CartItem
     */
    public function getItemWithProduct($product)
    {
       $items = $this->getItems()->filter(function(CartItem $item) use($product) {
            return $item->getProduct()->getId() == $product->getId();
        });

        return $items[0];
    }

    /**
     * @return int
     */
    public function getSumTotalQuantity()
    {
        return array_reduce(
            $this->getItems()->getValues(),
            function ($acc, $item) {
                $acc += $item->getQuantity();
                return $acc;
            }
        );
    }

    /**
     * @return int
     */
    public function getSumTotalPrice()
    {
        return array_reduce(
            $this->getItems()->getValues(),
            function ($acc, $item) {
                $acc += $item->getTotalPrice();
                return $acc;
            }
        );
    }
}
