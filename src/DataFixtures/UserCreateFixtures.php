<?php

namespace App\DataFixtures;

use App\Service\UserService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserCreateFixtures extends Fixture
{
    /** @var UserPasswordEncoderInterface  */
    private $encoder;

    /**
     * @var UserService
     */
    private $service;

    /**
     * UserCreateFixtures constructor.
     * @param UserService $service
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserService $service, UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
        $this->service = $service;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $object = new \stdClass();
        $object->firstName = "user";
        $object->lastName = "user";
        $object->username = "user";
        $object->email = "uveys@user.com";
        $object->password = "123321";
        $object->roles = ["ROLE_USER"];
        $this->service->create($object, $this->encoder);

        $object = new \stdClass();
        $object->firstName = "admin";
        $object->lastName = "admin";
        $object->username = "admin";
        $object->email = "uveys@admin.com";
        $object->password = "123321";
        $object->roles = ["ROLE_ADMIN"];
        $this->service->create($object, $this->encoder);
    }
}
