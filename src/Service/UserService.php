<?php


/**
 * Class UserService
 * @package App\Service
 * @author Üveys SERVETOĞLU <uveysservetoglu@gmail.com>
 */

namespace App\Service;


use App\Entity\User;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService extends AbstractService
{
    /**
     * @param $object
     * @param UserPasswordEncoderInterface $encoder
     * @throws \Exception
     */
    public function create($object, $encoder)
    {
        $userRepo = parent::getRepo(User::class);
        $checkUser = $userRepo->findOneBy(['email'=>'uveys@admin.com']);

        if (!is_null($checkUser)) {
            throw new \Exception('msg.error.user.already_exist');
        }

        $user = new User();
        $user->setFirstName($object->firstName);
        $user->setLastName($object->lastName);
        $user->setUsername($object->username);
        $user->setEmail($object->email);
        $user->setRoles($object->roles);
        $user->setPassword($encoder->encodePassword($user, $object->password));

        parent::persist($user);
    }
}
