<?php


/**
 * Class PaymentService
 * @package App\Service
 * @author Üveys SERVETOĞLU <uveysservetoglu@gmail.com>
 */

namespace App\Service;

use App\Entity\Cart;
use App\Entity\Payment;
use App\Entity\User;

class PaymentService extends AbstractService
{
    /**
     * @param User $user
     * @param Cart $cart
     * @param $content
     * @return string
     */
    public function payment($user, $cart, $content)
    {

        $today = date("Ymd");
        $rand = sprintf("%04d", rand(0,9999));
        $unique = $today . $rand;

        $payment = new Payment();
        $payment->setUser($user);
        $payment->setAddress(json_encode($content["address"]));
        $payment->setOrderNumber($unique);
        $payment->setPaymentType($content["paymentType"]);
        $payment->setStatus('pending');
        $payment->setCart($cart);

        $cart->setStatus('completed');
        parent::persist($payment, $cart);

        return $unique;
    }
}
