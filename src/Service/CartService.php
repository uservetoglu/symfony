<?php


/**
 * Class UserService
 * @package App\Service
 * @author Üveys SERVETOĞLU <uveysservetoglu@gmail.com>
 */

namespace App\Service;


use App\Entity\Cart;
use App\Entity\CartItem;
use App\Entity\Product;
use App\Entity\User;

class CartService extends AbstractService
{
    /**
     * @param Product $product
     * @param $quantity
     * @param User $user
     * @throws \Exception
     */
    public function addToCart($product, $quantity, $user)
    {
        /** @var Product $product */
        $product = parent::getRepo(Product::class)->find($product);

        if (is_null($product)) {
            throw new \Exception("msg.error.product.not_found", 400);
        }

        /** @var Cart $cart */
        $cart = self::checkCart($user);

        $cartItem = $cart->getItemWithProduct($product) ?? new CartItem();

        $cartItem->setCart($cart);
        $cartItem->setPrice($product->getPrice());
        $cartItem->setProduct($product);
        $cartItem->setQuantity($quantity < 0 ? $quantity: 1);
        $cartItem->setTotalPrice($product->getPrice() * $quantity);

        parent::persist($cartItem);
        self::cartUpdate($cart);
    }

    /**
     * @param User $user
     * @return object|null
     */
    public function checkCart($user)
    {
        $cart = parent::getRepo(Cart::class)->findOneBy(['user'=>$user, 'status'=>'active']);

        if (!is_null($cart)) {
            return $cart;
        }

        return self::emptyCart($user);
    }

    /**
     * @param $user
     * @return Cart
     */
    public function emptyCart($user)
    {
        $cart = new Cart();
        $cart->setUser($user);
        $cart->setTotalPrice(0);
        $cart->setTotalItems(0);
        $cart->setTotalQuantity(0);
        $cart->setStatus('active');
        parent::persist($cart);
        return $cart;
    }

    /**
     * @param Cart $cart
     */
    public function cartUpdate(Cart $cart)
    {
        $cart->setTotalQuantity($cart->getSumTotalQuantity());
        $cart->setTotalItems($cart->getItems()->count());
        $cart->setTotalPrice($cart->getSumTotalPrice());

        parent::persist($cart);
    }

    /**
     * @param CartItem $cartItem
     */
    public function removeItem($cartItem)
    {
        $cart = $cartItem->getCart();
        parent::remove($cartItem);
        self::cartUpdate($cart);
    }

    /**
     * @param $cart
     * @return int
     */
    public function getSumTotalQuantity($cart)
    {
        return array_reduce(
            $cart->getItems()->getValues(),
            function ($acc, $item) {
                $acc += $item->getQuantity();
                dump($acc);
                return $acc;
            }
        );
    }
}
