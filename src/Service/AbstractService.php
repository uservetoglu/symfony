<?php


/**
 * Class AbstractService
 * @package App\Service
 * @author Üveys SERVETOĞLU <uveysservetoglu@gmail.com>
 */

namespace App\Service;


use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractService
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var EntityManagerInterface
     */
    protected $em;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->em = $container->get('doctrine');
    }

    /**
     * @param $entity
     * @return \Doctrine\Persistence\ObjectRepository
     */
    protected function getRepo($entity)
    {
        return $this->em->getRepository($entity);
    }

    /**
     * @param array $entities
     */
    protected function persist(...$entities)
    {
        foreach ($entities as $entity) {
            $this->em->getManager()->persist($entity);
            $this->em->getManager()->flush();
        }
    }

    protected function remove($entity)
    {
        $this->em->getManager()->remove($entity);
        $this->em->getManager()->flush();
    }

    /**
     * @param string $cdnPath
     * @return string
     */
    protected function getUniqueImageName(string $cdnPath)
    {
        $imageName = uniqid() . rand(10, 99);
        if (file_exists($cdnPath.'/'.$imageName)) {
            return $this->getUniqueImageName($cdnPath);
        }
        return $imageName;
    }

}
