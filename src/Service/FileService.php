<?php

/**
 * Class FileService
 * @package App\Service\File
 * @author Üveys SERVETOĞLU <uveysservetoglu@gmail.com>
 */

namespace App\Service;

use App\Util\File\Resize;
use Symfony\Component\Filesystem\Filesystem;

class FileService extends AbstractService
{
    public $width = 275;
    public $height = 200;

    /**
     * @param $file
     * @param $path
     * @param bool $thumbnail
     * @return array
     * @throws \Exception
     */
    public function upload($file, $path, $thumbnail = false)
    {
        $fileSystem = new Filesystem();

        $result = [];
        $reqImage = is_array($file) ? $file : array($file);

        $cdnPath = $this->container->get('kernel')->getProjectDir().$path;

        if (!$fileSystem->exists($cdnPath)) {
            $fileSystem->mkdir($cdnPath."/thumb");
        }

        foreach ($reqImage as $uploadedFile) {
            try {
                $name = $this->getUniqueImageName($cdnPath);
                $imageName = $name.'.jpg';
                $uploadedFile->move($cdnPath, $imageName);
                $result[] = [
                    'fileName' => $imageName,
                    'url' => $name,
                    'type'=> 'JPG',
                    'cdnPath' => $path."/".$imageName,
                    'thumbPath' =>  $path.'/thumb/'.$imageName
                ];
                if ($thumbnail){
                    self::resize($cdnPath, $imageName);
                }
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }
        }
        return $result;
    }

    /**
     * @param $cdnPath
     * @param $imageName
     * @throws \Exception
     */
    public function resize($cdnPath, $imageName)
    {
        $thumbnail = new Resize();
        $thumbnail->image_canvas_x = $this->width;
        $thumbnail->image_canvas_y = $this->height;
        $thumbnail->newFile = $cdnPath.'/thumb/'.$imageName;
        $thumbnail->file = $cdnPath."/".$imageName;
        $thumbnail->process();
    }

    /**
     * @param $path
     * @param $imageName
     * @param $image
     * @return array
     * @throws \Exception
     */
    public function imageWrite($path, $imageName, $image)
    {
        $cdnPath = $this->container->get('kernel')->getProjectDir().$path;

        $fileSystem = new Filesystem();
        if (!$fileSystem->exists($cdnPath)) {
            $fileSystem->mkdir($cdnPath."/thumb");
        }

        $fp = fopen($cdnPath."/$imageName.jpg", 'w');
        fwrite($fp, $image);
        fclose($fp);

        self::resize($cdnPath, $imageName.'.jpg');

        $data = [
            'fileName' => $imageName,
            'url' => $imageName,
            'type'=> 'JPG',
            'cdnPath' => $cdnPath."/$imageName.jpg",
            'thumbPath' =>  $path.'/thumb/'."$imageName.jpg"
        ];

        return $data;
    }
}
