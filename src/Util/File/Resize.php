<?php

namespace App\Util\File;

class Resize
{
    /** @var array */
    private $gdInfo;
    private $image_x;
    private $image_y;
    private $mimeType;

    /**
     * Image Canvas Width Size
     */
    public $image_canvas_x;

    /**
     * Image Canvas Height Size
     */
    public $image_canvas_y;

    /**
     * Image path.file
     */
    public $file;

    /**
     * Image Saving Name
     */
    public $newFile;


    /**
     * @return bool
     * @throws \Exception
     */
    public function process()
    {
        $this->imageInfo();
        $this->checkMimeTypes();
        $this->createImage();
        return true;
    }


    public function __construct()
    {
        $this->checkGdInfo();
    }

    /**
     * Check GD Installed
     * @throws \Exception
     */
    private function checkGdInfo()
    {
        if (! function_exists('gd_info')) {
            throw new \Exception('Gd not installed');
        }

        $this->gdInfo = gd_info();
    }

    /**
     * Set Image Info
     */
    private function imageInfo()
    {
        $imageData = getimagesize($this->file);

        if (! $imageData) {
            throw new \Exception('Bad File');
        }

        $this->image_x = $imageData[0];
        $this->image_y = $imageData[1];
        $this->mimeType = $imageData['mime'];
    }

    /**
     * Check Image Mime Type
     * @return resource
     * @throws \Exception
     */
    private function checkMimeTypes()
    {
        switch ($this->mimeType) {
            case 'image/jpeg':
            case 'image/jpg':
                return $this->file = imagecreatefromjpeg($this->file);
                break;
            case 'image/png':
                return $this->file = imagecreatefrompng($this->file);
                break;
            default:
                throw new \Exception('Not Allowed File Type');
                break;
        }
    }

    /**
     * Create Canvas
     * @return resource
     * @throws \Exception
     */
    private function createCanvas()
    {
        try{
            $canvasImage = imagecreatetruecolor($this->image_canvas_x, $this->image_canvas_y);
            $white = imagecolorallocate($canvasImage, 0, 0, 0);

            imagefilledrectangle($canvasImage, 0, 0, $this->image_canvas_x, $this->image_canvas_y, $white);

            return $canvasImage;
        } catch(\Exception $err) {
            throw new \Exception('Failed to create canvas');
        }
    }

    /**
     * Calculate Dimensions
     * @return array
     */
    private function calculateDimensions()
    {
        $positionX = 0;
        $positionY = 0;
        $ratio = $this->image_x / $this->image_y;

        if ($this->image_y < $this->image_canvas_y && $this->image_x < $this->image_canvas_x) {
            $imageWidth = $this->image_x;
            $imageHeight = $this->image_y;
            $positionX = ($this->image_canvas_x - $imageWidth) / 2;
            $positionY = ($this->image_canvas_y - $imageHeight) / 2;
        } elseif ($ratio > 1) {
            $imageWidth = $this->image_canvas_x;
            $imageHeight = $imageWidth / $ratio;
            $positionY = ($this->image_canvas_y - $imageHeight) / 2;
        } else {
            $imageHeight = $this->image_canvas_y;
            $imageWidth = $this->image_x * $imageHeight / $this->image_y;
            $positionX = ($this->image_canvas_x - $imageWidth) / 2;
        }

        $data = array(
            'positionX' => $positionX,
            'positionY' => $positionY,
            'width' => $imageWidth,
            'height' => $imageHeight
        );

        return $data;
    }

    /**
     * Create New Image
     * @throws \Exception
     */
    private function createImage()
    {
        try{
            $canvas = $this->createCanvas();
            $dimensions = $this->calculateDimensions();

            imagecopyresampled(
                $canvas,
                $this->file,
                $dimensions['positionX'],
                $dimensions['positionY'],
                0,
                0,
                $dimensions['width'],
                $dimensions['height'],
                $this->image_x,
                $this->image_y
            );

            imagedestroy($this->file);

            imagejpeg($canvas, $this->newFile, 100);
            imagedestroy($canvas);
        } catch(\Exception $err) {
            throw new \Exception('Failed to create image');
        }
    }


}
