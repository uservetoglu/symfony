<?php

namespace App\Form;

use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProductFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',null,[
                'constraints'=> new NotBlank([
                  'message'=>'name can not be null'
                ])
            ])
            ->add('code',null,[
                'constraints'=> new NotBlank([
                    'message'=>'code can not be null'
                ])
            ])
            ->add('stock',null,[
                'constraints'=> new NotBlank([
                    'message'=>'stock can not be null'
                ])
            ])
            ->add('price',null,[
                'constraints'=> new NotBlank([
                    'message'=>'price can not be null'
                ])
            ])
            ->add('image', FileType::class, [
                'constraints'=> new NotBlank([
                    'message'=>'image can not be null'
                ])
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
