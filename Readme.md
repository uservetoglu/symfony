# Symfony 4.3 E-commerce case

### Installation
```sh
  $ composer install
  $ php bin/console doctrine:database:create
  $ php bin/console d:s:u --force
  $ php bin/console doctrine:fixtures:load
  $ symfony serve
```

### Explanation
Projede Symfony 4.3, Twig template engine, jquery, bootstrap ve vuejs kullanılmıştır.

Proje kurulumunu tamamladıktan sonra http://localhost:8000 tarayıcınızdan açın.
Eğer admin girişi yapıyorsanız kurulum esnasında oluşturduğumuz <b>uveys@admin.com</b> admin hesabı <b>123321</b> şifresiyle giriş yapınız.
 

### Route List

       app_register               ANY      ANY      ANY    /register                          
       app_login                  ANY      ANY      ANY    /login                             
       app_logout                 ANY      ANY      ANY    /logout          
       products                   GET      ANY      ANY    /admin/products                    
       dashboard                  ANY      ANY      ANY    /admin/dashboard                   
       product_new                ANY      ANY      ANY    /admin/products/new                
       product_edit               GET      ANY      ANY    /admin/products/{product}          
       product_update             ANY      ANY      ANY    /admin/products/{product}          
       product_delete             ANY      ANY      ANY    /admin/products/delete/{product}   
       home                       ANY      ANY      ANY    /                                  
       cart                       ANY      ANY      ANY    /cart                              
       add_to_cart                POST     ANY      ANY    /addToCart                         
       cart_item_remove           ANY      ANY      ANY    /removeCartItem/{cartItem}         
       checkout                   ANY      ANY      ANY    /checkout                          
       payment                    ANY      ANY      ANY    /payment                           
       payment_patch              PATCH    ANY      ANY    /payment/{payment}                 
       home_products              GET      ANY      ANY    /product-list                      


